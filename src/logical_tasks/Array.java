package logical_tasks;

public class Array {

    private int [] array1;
    private int [] array2;

    public Array(int [] array1, int [] array2){
        this.array1 = array1;
        this.array2 = array2;
    }

    public void duplicateElementsArray(){

        int [] array3;
        int countDuplicate = 0;
        int counter = 0;

        array3 = new int[numberOfDuplicated()];

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if(array1[i] == array2[j]) {
                    array3[counter] = array1[i];
                    counter++;
                    break;
                }
            }
        }

        System.out.println("Number of duplicate: " + numberOfDuplicated());
        System.out.print("Array with duplicates: ");

        for(int a: array3) {
            System.out.print(a + " ");
        }
    }

    public int numberOfDuplicated(){

        int countDuplicate = 0;

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if(array1[i] == array2[j]) {
                    countDuplicate++;
                    break;
                }
            }
        }

        return countDuplicate;
    }

    public void elementsExistInOneArray(){

        int [] array3 = new int[numberOfUniqueElements()];
        int counter = 0;


        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if(array1[i] != array2[j] && j == i) {
                    array3[counter] = array1[i];
                    counter++;
                    break;
                }
            }
        }

        System.out.println("\nNumber of no duplicated: " + numberOfUniqueElements());
        System.out.print("Array with no duplicates: ");

        for(int a: array3) {
            System.out.print(a + " ");
        }
    }

    public int numberOfUniqueElements() {

        boolean contains = false;
        int count = 0;

        for(int i=0; i< array1.length; i++) {
            for(int j=0; j< array2.length; j++) {
                if (array1[i] == array2[j]) {
                    contains = true;
                    break;
                }
            }
            if(!contains) {
                count++;
            } else {
                contains = false;
            }
        }

        return count;
    }

    private static void print(int [] c){
        for(int i = 0; i < c.length; i++){
            System.out.print(c[i] + " ");
        }
    }

    private void deleteDuplicated(int [] array) throws ArrayIndexOutOfBoundsException {

        int [] temp = new int[array.length];
        int countRepeat = 0;
        int counter = 0;

        for (int i = 0; i < array.length-1; i++) {
            for (int j = i+1; j < array.length; j++) {
                if (array[i]==array[j]){
                    if(countRepeat>2) {
                        break;
                    }
                    countRepeat++;
                }else {
                    if(j==array.length-1){
                        temp[counter]=array[i];
                        counter++;
                    }
                }
            }
        }

        print(temp);
    }

    public static void main(String[] args){

        int [] a = {1, 2, 3, 4, 5, 7};
        int [] b = {5, 2, 6, 7, 5, 7, 7};

        Array array = new Array(a, b);

        array.duplicateElementsArray();
        array.elementsExistInOneArray();

        array.deleteDuplicated(b);

    }
}
