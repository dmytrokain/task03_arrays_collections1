package logical_tasks;

import java.util.Arrays;

public class Doors {

    static int health = 25;
    static int [] doors = new int[10];


    public static int monsterPower(){
        return 5 + (int) (Math.random() * 100);
    }

    public static int artifactPower(){
        return 10 + (int) (Math.random() * 80);
    }

    public static void buildDoors(){

        doors[0] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;
        doors[1] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;
        doors[2] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;
        doors[3] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;
        doors[4] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;
        doors[5] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;
        doors[6] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;
        doors[7] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;
        doors[8] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;
        doors[9] = 1 + (int) (Math.random() * 50) > 25 ? 1 : 0;

    }

    public static void game(){

        for(int i = 1; i < doors.length; i++){
            if(doors[i] == 1){
                System.out.println(i + " Artifact + " + artifactPower());
                health += artifactPower();
            } else {
                System.out.println(i + " Monster - " + monsterPower());
                health -= monsterPower();
            }
        }

        if(health < 0){
            System.out.println("\nHealth: " + health
                    + " it's death!");
        } else {
            System.out.println("\nHealth: " + health
                    + " you win!");
        }
    }


    public static void main(String[] args) {

        buildDoors();
        game();

    }
}
