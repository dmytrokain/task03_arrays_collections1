package Generics;

public class Main {

    public static void main(String[] args) {
        Garage<Car> garage = new Garage<>();
        Car lambogrini = new Car("lamborgini");

        garage.getCars().add(lambogrini);

        System.out.println(garage.getCars());
    }
}
