package Generics;

import java.util.ArrayList;
import java.util.Collection;

public class Garage<T> {

    private Collection<T> cars;

    public Garage(){
        cars = new ArrayList<>();
    }

    public Collection<T> getCars(){
        return cars;
    }

    public void setCars(Collection<T> cars){
        this.cars = cars;
    }

}
